package dev.steve.service2.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Service2Controller {

    @GetMapping("service2")
    public String service(){
        return "Service 2 - UwU";
    }

}
